<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Likes;
use App\Http\Resources\LikesResource;
use App\Http\Resources\LikesCollection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class LikesController extends Controller
{
    public function index(Request $request){
        $likes = Likes::where("user_id", $request->user_id)
                    ->get();

        return new LikesCollection($likes);
    }

    public function show(Request $request, $id){

    }

    public function store(Request $request){
        try{
            $record = Likes::withTrashed()
                        ->where("user_id", $request->user_id)
                        ->where("imdb_id", $request->imdb_id)
                        ->first();

            if($record){
                $record->restore();
                return response()->json([
                    'message' => 'liked'
                ], 200);
            } else {
                $likes = new Likes;
                $likes->fill($request->all());

                DB::transaction(function() use($likes, $request){
                    $likes->saveOrFail();
                });

                return response()->json([
                    'id' => $likes->id,
                    'created_at' => $likes->created_at
                ], 201);
            }

            
        }
        catch (QueryException $ex){
            return response()->json([
                'errors' => $ex->getMessage(),
            ], 500);
        }
        catch(Exception $ex){
            return response()->json([
                'errors' => $ex->getMessage(),
            ], 500);
        }
    }

    public function update(Request $request, $id){

    }

    public function destroy(Request $request, $id){
        $message = "failed to delete";

        $likes = Likes::where("user_id", $request->user_id)
                    ->where("imdb_id", $request->imdb_id)
                    ->first();
        
        if($likes){
            if($likes->delete()){
                $message = "deleted";
            }
        }

        return response()->json([
            'message' => $message,
        ]);
    }
}
